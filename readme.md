# Asteroids Web

Asteroids is a small project that integrates most of the core features of ReactJS and full integration with a Unity game.


## Next Steps

- Configure gzip compression in the web server.
- Add conditional rendering to show some feedback for users coming from mobiles.
- Measure the best max time for Unity to load the indexedDB cache.
- Configure environment variables for secret keys and exclude them from git.
- Cross-browser and cross-device testing.
- Jest's unit tests and Cypress' automated tests.