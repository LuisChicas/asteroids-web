import database from '../firebase/firebase';

export const setLeaderboard = (highscores) => ({
    type: 'SET_LEADERBOARD',
    highscores
});

export const startSetLeaderboard = (size) => {
    return (dispatch) => {
        return database.ref('highscores').on('value', (snapshot) => {
            let highscores = [];

            snapshot.forEach((childSnapshot) => {
                highscores.push({
                    id: childSnapshot.key,
                    ...childSnapshot.val()
                });
            });

            highscores.sort((a, b) => b.highscore - a.highscore);

            if (highscores.length > size) {
                highscores = highscores.slice(0, size);
            }

            dispatch(setLeaderboard(highscores));
        });
    };
};

export const uploadHighscore = () => ({
    type: 'UPLOAD_HIGHSCORE'
});

export const startUploadHighscore = (user = {}) => {
    return (dispatch) => {
        return database.ref('highscores').push(user).then((ref) => {
            dispatch(uploadHighscore({
                id: ref.key,
                ...user
            }));
        });
    };
};