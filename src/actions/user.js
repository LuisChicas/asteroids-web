export const fetchUserData = () => {
    const name = localStorage.getItem('name') || '';
    const highscore = localStorage.getItem('highscore') || '0';

    return {
        type: 'FETCH_USER_DATA',
        name,
        highscore: parseInt(highscore)
    };
};

export const setName = (name) => {
    localStorage.setItem('name', name);

    return {
        type: 'SET_NAME',
        name
    }
};

export const setHighscore = (highscore) => {
    localStorage.setItem('highscore', highscore);

    return {
        type: 'SET_HIGHSCORE',
        highscore
    }
};