import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import configureStore from './store/configureStore';
import AppRouter from './routers/AppRouter';
import { fetchUserData } from './actions/user';
import { setLeaderboard, startSetLeaderboard } from './actions/scores';
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'normalize.css/normalize.css';
import './styles/styles.scss';

const store = configureStore();

const app = (
    <Provider store={ store }>
        <AppRouter />
    </Provider>
);

store.dispatch(fetchUserData());

const leaderboardSize = 6;
store.dispatch(startSetLeaderboard(leaderboardSize));

ReactDOM.render(app, document.getElementById('app'));