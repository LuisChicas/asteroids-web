import React from 'react';

export default () => {
    const year = new Date().getFullYear();

    return (
        <footer className="border-top footer text-muted py-2">
            <div className="ast-footer container">
                <div className="footer-info">
                    &copy; {year} - Asteroids
                    <span style={{fontSize: "0.9rem"}}> by <a href="https://connect.unity.com/u/luis-chicas">Luis Chicas</a></span>
                </div>
            </div>
        </footer>
    );
} 