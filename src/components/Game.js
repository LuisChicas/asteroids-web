import React from 'react';
import Unity, { UnityContent } from "react-unity-webgl";
import { connect } from 'react-redux';
import { setHighscore } from '../actions/user';
import { startUploadHighscore } from '../actions/scores';

const unityContent = new UnityContent(
    "game/game.json",
    "game/UnityLoader.js"
);

const Game = (props) => {
    if (props.name) {
        unityContent.send(
            "UserManager",
            "SetUsername",
            props.name
        );

        unityContent.send(
            "UserManager",
            "SetHighscore",
            props.highscore
        );
    }

    unityContent.on("loaded", () => {
        if (props.name) {
            unityContent.send(
                "UserManager",
                "SetUsername",
                props.name
            );
    
            unityContent.send(
                "UserManager",
                "SetHighscore",
                props.highscore
            );
        }
    });

    unityContent.on("OnNewHighscore", (score) => {
        props.setHighscore(score);
        props.startUploadHighscore({ 
            name: props.name, 
            highscore: score 
        });
    });

    return (
        <Unity unityContent={unityContent} />
    );
};

const mapStateToProps = (state) => ({
    name: state.user.name,
    highscore: state.user.highscore
});

const mapDispatchToProps = (dispatch) => ({
    setHighscore: (highscore) => dispatch(setHighscore(highscore)),
    startUploadHighscore: (user) => dispatch(startUploadHighscore(user))
});

export default connect(mapStateToProps, mapDispatchToProps)(Game);