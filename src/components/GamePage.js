import React from 'react';
import { MDBContainer, MDBRow, MDBCol } from 'mdbreact';
import Game from './Game';
import Leaderboard from './Leaderboard';

export default class GamePage extends React.Component {
    
    render() {
        return (
            <MDBContainer className="game-container" fluid>
                <MDBRow>
                    <MDBCol size="6" className="offset-3 px-0">
                        <div className="game">
                            <Game />
                        </div>
                    </MDBCol>
                    <MDBCol size="2" middle className="offset-1 px-0">
                        <Leaderboard title={"Profile"} />
                    </MDBCol>
                </MDBRow>
            </MDBContainer>
        );
    }
};