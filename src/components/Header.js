import React from 'react';
import { Link } from 'react-router-dom';

export default () => {
    return (
        <div className="ast-navbar shadow-sm d-flex align-items-center">
            <Link to="/">
                <img src="/images/ship.png" className="mr-3" width="35" height="30" />
            </Link>
            <Link to="/" className="mr-auto">
                <h4 className="my-0">
                    Asteroids
                </h4>
            </Link>
        </div>
    );
};
