import React from 'react';
import { connect } from 'react-redux';
import NameInput from './NameInput';

const Leaderboard = (props) => {
	const maxRows = 8;

	const highscores = props.highscores || [];
	const rows = new Array(highscores.length > maxRows ? maxRows : highscores.length).fill(0).map((z, i) => (
		<li key={i}>
			<mark>{highscores[i].name}</mark>
			<small>{highscores[i].highscore}</small>
		</li>
	));
	
	return (
		<div className='leaderboard'>
			<div className='leaderboard-header'>
				<h1>{props.title || 'Leaderboard'}</h1>
				<NameInput />
				<h2>Highscore: {props.highscore}</h2>
			</div>
			<ol>{rows}</ol>
		</div>
	);
}

const mapStateToProps = (state) => ({
	highscore: state.user.highscore,
	highscores: state.scores.highscores
});

export default connect(mapStateToProps)(Leaderboard);