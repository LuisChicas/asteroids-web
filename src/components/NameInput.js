import React from 'react';
import { connect } from 'react-redux';
import { MDBInputGroup, MDBIcon } from 'mdbreact';
import { setName } from '../actions/user';

class NameInput extends React.Component {
    state = {
        name: this.props.name,
        isInEditMode: !this.props.name
    };

    onInputChange = (e) => {
        const name = e.target.value;

        if (name.match(/^[a-zA-Z0-9-_]*$/))
            this.setState({ name });
    };

    onNameSet = (e) => {
        const name = this.state.name;
        
        if (name) {
            this.props.setName(name);
            this.setState({ isInEditMode: false });
        }
    };

    onEditEnable = () => { 
        this.setState({ isInEditMode: true });
    };

    render() {
        let onClick, buttonIcon, inputClass;

        if (this.state.isInEditMode) {
            onClick = this.onNameSet;
            buttonIcon = "check";
            inputClass = '';
        } else {
            onClick = this.onEditEnable;
            buttonIcon = "pen";
            inputClass = "disabled name-input-disabled";
        }

        return (
            <MDBInputGroup
                type="text"
                className={inputClass}
                containerClassName="pb-2"
                hint="Your name"
                append={
                    <button 
                        className="btn btn-info m-0 py-0 px-3"
                        onClick={onClick}>
                        <MDBIcon icon={buttonIcon} />
                    </button>
                }
                value={this.state.name}
                onChange={this.onInputChange}                 
            />
        );
    }
}

const mapStateToProps = (state) => ({
    name: state.user.name
});

const mapDispatchToProps = (dispatch) => ({
    setName: (name) => dispatch(setName(name))
});

export default connect(mapStateToProps, mapDispatchToProps)(NameInput);