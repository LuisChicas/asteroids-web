import * as firebase from 'firebase/app';
import 'firebase/database';

const config = {
    apiKey: "AIzaSyCIWc2rO4KWBVaHMjHkD1fHpqqVzeDQPTo",
    authDomain: "asteroids-web.firebaseapp.com",
    databaseURL: "https://asteroids-web.firebaseio.com",
    projectId: "asteroids-web",
    storageBucket: "asteroids-web.appspot.com",
    messagingSenderId: "524255272897",
    appId: "1:524255272897:web:bab6ee69ec88b9cc8b1940"
};

firebase.initializeApp(config);

const database = firebase.database();

export { firebase, database as default };