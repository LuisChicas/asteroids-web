const defaultScoresData = {
    highscores: []
};

export default (state = defaultScoresData, action) => {
    switch (action.type) {
        case 'SET_LEADERBOARD':
            return {
                ...state,
                highscores: action.highscores
            };
        case 'UPLOAD_HIGHSCORE':
            return state;
        default:
            return state;
    }
};