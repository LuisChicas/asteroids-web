const defaultUserData = {
    name: '',
    highscore: 0
};

export default (state = defaultUserData, action) => {
    switch (action.type) {
        case 'SET_NAME':
            return {
                ...state, 
                name: action.name
            };
        case 'SET_HIGHSCORE':
            return {
                ...state, 
                highscore: action.highscore
            };
        case 'FETCH_USER_DATA':
            return {
                name: action.name,
                highscore: action.highscore
            };
        default:
            return state;
    }
};