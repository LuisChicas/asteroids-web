import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Header from '../components/Header';
import Footer from '../components/Footer';
import GamePage from '../components/GamePage';

export default () => (
    <BrowserRouter>
        <div className="content">
            <Header />
            <Switch>
                <Route path="/" component={ GamePage } exact={true}/>
            </Switch>
            <Footer />
        </div>
    </BrowserRouter>
);